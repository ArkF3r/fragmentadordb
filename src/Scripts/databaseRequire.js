const mysql = require('mysql');

class DatabaseConnector {
  constructor(host, user, password) {
    this.connection = mysql.createConnection({
      host: host,
      user: user,
      password: password
    });
    this.connection.connect();
  }
  close() {
    this.connection.end();
  }
  getDatabases() {
    return new Promise(resolve => {
      this.connection.query("show databases;", function (
        error,
        results,
        fields
      ) {
        if (error) throw error;
        var dbs = [];
        for (var database of results) {
          dbs.push(database.Database);
        }
        resolve(dbs);
      });
    });
  }
  getTables() {
    return new Promise(resolve => {
      this.connection.query("show tables;", function (error, results, fields) {
        if (error) throw error;
        var tables = [];
        for (var table of results) {
          tables.push(table.Tables_in_world);
        }
        resolve(tables);
      });
    });
  }
  getTablesRelation() {
    return new Promise(resolve => {
      this.connection.query("show tables;", (error, results, fields) => {
        if (error) throw error;
        var promises = []
        for (var table of results) {
          //var nombreTabla = table.Tables_in_world
          var nombreTabla = table[`${fields[0].name}`]
          promises.push(this.getTableDescription(nombreTabla))
        }
        Promise.all(promises).then((elems) => {
          resolve(elems)
        })
      });
    });
  }
  getTableDescription(nTabla) {
    return new Promise(resolve => {
      this.connection.query(
        "SHOW COLUMNS FROM " + nTabla + ";",
        (error, results) => {
          if (error) throw error;
          var columns = [];
          for (var column of results) {
            var colm = JSON.parse(JSON.stringify(column))
            var key = ""
            if (colm.Key === "")
              key = null
            else
              key = colm.Key
            columns.push({
              nombre: colm.Field,
              tipo: colm.Type,
              key: key
            });
          }
          resolve({
            nombre: nTabla,
            atributos: columns
          });
        }
      );
    });
  }
  setDatabase(database) {
    return new Promise(resolve => {
      this.connection.query("use " + database + ";", () => {
        resolve(1);
      });
    });
  }
  runQuery(query){
    return new Promise((resolve)=>{
      this.connection.query(
        query,
        function(error, results,fields){
          if (error) throw error;
          resolve({results:results,fields:fields});
        }
      )
    })
  }
  getDataToInsert(query){
    return new Promise((resolve)=>{
        database.runQuery(query).then((data)=>{
            var atribs = []
            for(var field of  data.fields){
                atribs.push(field.name)
            }
            var datos = ""
            for(var res of data.results){
                datos+="("
                for(var i=0;i<atribs.length;i++){
                    datos+=res[atribs[i]]+","
                }
                datos=datos.slice(0,datos.length-1)
                datos+="),"
            }
            datos=datos.slice(0,datos.length-1)
            datos+=";"
            database.close()
            resolve(data)
        })
    })
  }
}
/*
var database = new DatabaseConnector("47.254.93.16", "user", "password");
/*database.getDatabases().then(dbs => {
  console.log(dbs);
});
database.setDatabase("world").then(() => {
  console.log("Base elejida");
});
database.getTables("world").then(tables => {
  console.log(tables);
});
database.getTablesRelation("city").then((tables) => {
  console.log(tables)
})
database.getTableDescription("city").then((data) => {
  console.log(data)
})
*/
module.exports=DatabaseConnector