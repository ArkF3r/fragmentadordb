const mysql = require("mysql");

export default class DatabaseConnector {
  constructor(host, user, password, port = 3306) {
    this.connection = mysql.createConnection({
      host: host,
      port: port,
      user: user,
      password: password
    });
    this.connection.connect();
  }
  close() {
    this.connection.end();
  }
  getDatabases() {
    return new Promise(resolve => {
      this.connection.query("show databases;", function(
        error,
        results,
        fields
      ) {
        if (error) throw error;
        var dbs = [];
        for (var database of results) {
          dbs.push(database.Database);
        }
        resolve(dbs);
      });
    });
  }
  getTables() {
    return new Promise(resolve => {
      this.connection.query("show tables;", function(error, results, fields) {
        if (error) throw error;
        var tables = [];
        for (var table of results) {
          tables.push(table.Tables_in_world);
        }
        resolve(tables);
      });
    });
  }
  getTablesRelation() {
    return new Promise(resolve => {
      this.connection.query("show tables;", (error, results, fields) => {
        if (error) throw error;
        var promises = [];
        for (var table of results) {
          //var nombreTabla = table.Tables_in_world
          var nombreTabla = table[`${fields[0].name}`];
          promises.push(this.getTableDescription(nombreTabla));
        }
        Promise.all(promises).then(elems => {
          resolve(elems);
        });
      });
    });
  }
  getTableDescription(nTabla) {
    return new Promise(resolve => {
      this.connection.query(
        "SHOW COLUMNS FROM " + nTabla + ";",
        (error, results) => {
          if (error) throw error;
          var columns = [];
          for (var column of results) {
            var colm = JSON.parse(JSON.stringify(column));
            var key = "";
            if (colm.Key === "") key = null;
            else key = colm.Key;
            columns.push({
              nombre: colm.Field,
              tipo: colm.Type,
              key: key
            });
          }
          resolve({
            nombre: nTabla,
            atributos: columns
          });
        }
      );
    });
  }
  setDatabase(database) {
    return new Promise(resolve => {
      this.connection.query("use " + database + ";", () => {
        resolve(1);
      });
    });
  }
  runQuery(query) {
    return new Promise(resolve => {
      this.connection.query(query, function(error, results, fields) {
        if (error) throw error;
        resolve({ results: results, fields: fields });
      });
    });
  }
  executeQuery(query) {
    return new Promise(resolve => {
      this.connection.query(query, function(error, results, fields) {
        if (error) throw error;
        resolve(1);
      });
    });
  }
  getDataToInsert(insert, query) {
    return new Promise(resolve => {
      this.runQuery(query).then(data => {
        var atribs = [];
        for (var field of data.fields) {
          atribs.push({ name: field.name, charsetNr: field.charsetNr });
        }
        var datos = "";
        for (var res of data.results) {
          datos += "(";
          for (var i = 0; i < atribs.length; i++) {
            if (atribs[i].charsetNr == 33) {
              datos += '"' + res[atribs[i].name] + '",';
            } else {
              datos += res[atribs[i].name] + ",";
            }
          }
          datos = datos.slice(0, datos.length - 1);
          datos += "),";
        }
        datos = datos.slice(0, datos.length - 1);
        datos += ";";
        resolve(insert + " " + datos);
      });
    });
  }
}
/*
var database = new DatabaseConnector("47.254.93.16", "user", "password");
/*database.getDatabases().then(dbs => {
  console.log(dbs);
});
database.setDatabase("world").then(() => {
  console.log("Base elejida");
});
database.getTables("world").then(tables => {
  console.log(tables);
});
database.getTablesRelation("city").then((tables) => {
  console.log(tables)
})
database.getTableDescription("city").then((data) => {
  console.log(data)
})
*/
