const getRelations = () => {
  //Realiza una peticion al servidor para conocer las relaciones.
  var REL = [{
      nombre: `Relacion1`,
      atributos: [{
          nombre: `R1Atr1`,
          tipo: `varchar(50)`,
          key: `PRI`
        },
        {
          nombre: `R1Atr2`,
          tipo: `int(11)`,
          key: null
        },
        {
          nombre: `R1Atr3`,
          tipo: `varchar(50)`,
          key: null
        }
      ]
    },
    {
      nombre: `Relacion2`,
      atributos: [{
          nombre: `R2Atr1`,
          tipo: `int(11)`,
          key: `PRI`
        },
        {
          nombre: `R2Atr2`,
          tipo: `int(11)`,
          key: null
        },
        {
          nombre: `R2Atr3`,
          tipo: `varchar(50)`,
          key: null
        }
      ]
    },
    {
      nombre: `Relacion3`,
      atributos: [{
          nombre: `R3Atr1`,
          tipo: `varchar(50)`,
          key: `PRI`
        },
        {
          nombre: `R3Atr2`,
          tipo: `int(11)`,
          key: null
        },
        {
          nombre: `R3Atr3`,
          tipo: `varchar(50)`,
          key: null
        },
        {
          nombre: `R3Atr4`,
          tipo: `int(11)`,
          key: null
        },
        {
          nombre: `R3Atr5`,
          tipo: `varchar(50)`,
          key: `MUL`
        }
      ]
    }
  ];
  return REL;
};

const getSelectOptions = Relations => {
  var Options = [];
  Relations.forEach((element, index) => {
    Options.push({
      option: element.nombre,
      value: index
    });
  });
  return Options;
};

const getSitios = () => {
  var Sitios = [{
      nombre: `Sitio 1`,
      id: 1
    },
    {
      nombre: `Sitio 2`,
      id: 2
    },
    {
      nombre: `Sitio 3`,
      id: 3
    },
    {
      nombre: `Sitio 4`,
      id: 4
    }
  ];
  return Sitios;
};

//export default { getRelations, getSelectOptions };
export {
  getRelations,
  getSelectOptions,
  getSitios
};