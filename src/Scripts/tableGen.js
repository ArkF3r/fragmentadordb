//var dumper = require("./databasedumper")
function tableGen(frags) {
  var querys = [];
  console.log(frags);
  for (var frag of frags) {
    var query = "create table IF NOT EXISTS " + frag.relacion + "(";
    for (var atributo of frag.atributos) {
      query +=
        atributo.nombre +
        " " +
        atributo.tipo +
        " " +
        isPrimary(atributo.key) +
        ",";
    }
    query = query.slice(0, query.length - 1) + ");";
    querys.push(query);
  }
  return querys;
}

function SingletableGen(atributos) {
  var query = "create table IF NOT EXISTS " + frag.relacion + "(";
  for (var atributo of atributos) {
    query +=
      atributo.nombre +
      " " +
      atributo.tipo +
      " " +
      isPrimary(atributo.key) +
      ",";
  }
  query = query.slice(0, query.length - 1) + ");";
  return query;
}

function databaseGen(frags) {
  var databases = []; //new Set();
  for (var fragmento of frags) {
    // databases.add(
    //   "create database IF NOT EXISTS     " + fragmento.actual + ";"
    // );
    databases.push(
      "create database IF NOT EXISTS     " + fragmento.actual + ";"
    );
  }
  return databases; //Array.from(databases);
}

function isPrimary(key) {
  if (key === "PRI") {
    return "primary key";
  } else {
    return "";
  }
}

function dataDumper(frags) {
  var querys = [];
  for (var frag of frags) {
    var atributos = "";
    for (var atributo of frag.atributos) {
      atributos += atributo.nombre + ",";
    }
    atributos = atributos.slice(0, atributos.length - 1);
    var query =
      "select " +
      atributos +
      " from " +
      frag.actual +
      "." +
      frag.relacion +
      ";";
    querys.push(query);
  }
  return querys;
}

function insertInto(frags) {
  var querys = [];
  for (var frag of frags) {
    var atribs = "";
    for (var atributo of frag.atributos) {
      atribs += atributo.nombre + ",";
    }
    atribs = atribs.slice(0, atribs.length - 1);
    var query =
      "insert into " +
      frag.actual +
      "." +
      frag.relacion +
      "(" +
      atribs +
      ") values ";
    querys.push(query);
  }
  return querys;
}

export { tableGen, databaseGen, dataDumper, insertInto, SingletableGen };
