function QueryGenerator(tipo, atributos, relacion) {
  switch (tipo) {
    case 1: {
      //Horizontal
      return HorizontalQuery(atributos, relacion);
    }
    case 2: {
      //Vertical
      return VerticalQuery(atributos, relacion);
    }
    case 3: {
      //Hibrida
      break;
    }
    default: {
      throw console.error("Error");
    }
  }
}

function HorizontalQuery(atributo, relacion) {
  var query = "select * from " + relacion + " where";
  query =
    query +
    " " +
    atributo.atributo +
    " " +
    atributo.operador +
    " " +
    isA(atributo.valor) +
    " ";
  return query + ";";
}

function VerticalQuery(atributos, relacion) {
  var atribs = "";
  for (var atributo of atributos) {
    atribs = atribs + " " + atributo + ",";
  }
  atribs = atribs.substring(0, atribs.length - 1);
  return "select " + atribs + " from " + relacion + ";";
}

function isA(dato) {
  if (isNaN(dato)) {
    return "'" + dato + "'";
  } else {
    return dato;
  }
}
function whatIS(valor,tipo){
  if(tipo==="cadena"){
    return "'"+valor+"'"
  }else{
    return valor
  }
}

function queryConcat(predicados,relacion){
  var query = "select * from "+relacion+" where "
  for(var predicado of predicados){
    query+=" "+predicado.atributo+" "+predicado.operador+" "+whatIS(predicado.valor,predicado.tipo)+" and"
  }
  query=query.slice(0,query.lastIndexOf("and"))
  query+=";"
  return query
}


export {QueryGenerator,queryConcat}
/*
var atributos = [
  { opt: "", atributo: "nombre", operador: "LIKE", valor: "Eric" },
  { opt: "and", atributo: "nombre", operador: "LIKE", valor: "Raul" },
  { opt: "and", atributo: "nombre", operador: "LIKE", valor: "Pedro" }
];
var atributos = ["nombre", "apPaterno", "apMaterno"];
var tipo = 2;
var relacion = "alumno";

var query = QueryGenerator(tipo, atributos, relacion);
console.log(query);
*/