function Negativer(predicate) {
  var id = predicate.id.slice(0, predicate.id.length - 1);
  var newpredicate = {
    status: predicate.status,
    atributo: predicate.atributo,
    operador: predicate.operador,
    valor: predicate.valor,
    id: id + "B"
  };
  switch (predicate.operador) {
    case "=": {
      newpredicate.operador = "<>";
      break;
    }
    case "<=": {
      newpredicate.operador = ">";
      break;
    }
    case ">=": {
      newpredicate.operador = "<";
      break;
    }
    case "<>": {
      newpredicate.operador = "=";
      break;
    }
    case "<": {
      newpredicate.operador = ">=";
      break;
    }
    case ">": {
      newpredicate.operador = "<=";
      break;
    }
    case "like": {
      newpredicate.operador = "not like";
      break;
    }
    case "not like": {
      newpredicate.operador = "like";
      break;
    }
  }
  return newpredicate;
}

export { Negativer };
